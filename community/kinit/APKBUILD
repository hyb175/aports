# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kinit
pkgver=5.108.0
pkgrel=2
pkgdesc="Process launcher to speed up launching KDE applications"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	kconfig-dev
	kcrash-dev
	ki18n-dev
	kio-dev
	kservice-dev
	kwindowsystem-dev
	libcap-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kinit-$pkgver.tar.xz"
options="!check suid" # No tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kinit.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fffd13ffc90eb022597f457cd4d738ec9fe864ab4453fb27473190b903082d9f67746feb529acc1299e7e342bbfd1265b14dcbfab70c3be621d264f878d20dad  kinit-5.108.0.tar.xz
"
