# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-other
pkgname=okteta
pkgver=0.26.12
pkgrel=0
pkgdesc="KDE hex editor for viewing and editing the raw data of files"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.okteta"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND (GPL-2.0-only OR GPL-3.0-only) AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kbookmarks-dev
	kcmutils-dev
	kcodecs-dev
	kcompletion-dev
	kconfigwidgets-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	kparts-dev
	kservice-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qca-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtscript-dev
	qt5-qttools-dev
	qt5-qtxmlpatterns-dev
	samurai
	shared-mime-info
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/okteta/$pkgver/src/okteta-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESKTOPPROGRAM=ON
	cmake --build build
}

check() {
	xvfb-run ctest -j1 --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
50159269099e08bf95e4d358f9f8fbf3fb87e1f01888d5f78a1bdea83a2b56145e95c42a67eb3c82a7525b1352eb3ea1bf93187e8df0f06f35f6834a54ffa892  okteta-0.26.12.tar.xz
"
