# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=prison
pkgver=5.108.0
pkgrel=2
pkgdesc="A barcode API to produce QRCode barcodes and DataMatrix barcodes"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://community.kde.org/Frameworks"
license="MIT"
depends_dev="
	libdmtx-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	zxing-cpp-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	libqrencode-dev
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/prison-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/prison.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# prison-datamatrixtest and prison-qrtest are broken on s390x
	case "$CARCH" in
		s390x) ctest --test-dir build --output-on-failure -E "prison-(datamatrix|qr)test" ;;
		*) ctest --test-dir build --output-on-failure ;;
	esac
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2923f4ecbc81ce093e2be38c98726dd62c74e7783aabf67e4e62858aa7002228f2da69d27150a5f95648c304f56114758c0f8215d5bec442584e2d22b4440120  prison-5.108.0.tar.xz
"
