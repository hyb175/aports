# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-beartype
_pyname=beartype
pkgver=0.15.0
pkgrel=0
pkgdesc="Fast, pure-Python type checker"
url="https://github.com/beartype/beartype"
arch="noarch"
license="MIT"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-numpy py3-mypy py3-docutils py3-sphinx"
subpackages="$pkgname-pyc"
source="
	$_pyname-$pkgver.tar.gz::https://github.com/beartype/beartype/archive/refs/tags/v$pkgver.tar.gz
	"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	# nuitka isn't packaged for alpine yet
	# pyright isn't available on all architectures
	# long type-check test fails on some architectures
	python3 -m venv --clear --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest \
		--deselect beartype_test/a90_func/z90_lib/a90_nuitka/test_nuitka.py \
		--deselect beartype_test/a90_func/pep/test_pep561_static.py::test_pep561_pyright \
		--deselect beartype_test/a00_unit/a90_decor/test_decorwrapper.py::test_wrapper_fail_obj_large
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
1d797e756ebb6de71eeefd657b1339f9a716d7dcfdb398255bddba9a2629d87d0e848bf4b3156e6f859dee4af1498db7547bffbb712ebe8d9b68f2e48719130b  beartype-0.15.0.tar.gz
"
