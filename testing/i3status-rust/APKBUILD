# Contributor: Galen Abell <galen@galenabell.com>
# Contributor: Maxim Karasev <begs@disroot.org>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=i3status-rust
pkgver=0.31.9
pkgrel=0
pkgdesc="i3status replacement in Rust"
url="https://github.com/greshake/i3status-rust"
arch="all !s390x !riscv64" # limited by cargo
license="GPL-3.0-only"
makedepends="
	cargo
	cargo-auditable
	curl-dev
	dbus-dev
	lm-sensors-dev
	notmuch-dev
	openssl-dev>3
	pulseaudio-dev
	"
options="net"
provides="i3status-rs=$pkgver-r$pkgrel"
subpackages="$pkgname-doc"
source="
	https://github.com/greshake/i3status-rust/archive/refs/tags/v$pkgver/i3status-rust-v$pkgver.tar.gz
	https://dev.alpinelinux.org/archive/i3status-rs/i3status-rs-$pkgver.1
	"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --all-features
}

package() {
	install -Dm755 target/release/i3status-rs -t "$pkgdir"/usr/bin/

	install -Dm644 "$srcdir"/i3status-rs-$pkgver.1 \
		"$pkgdir"/usr/share/man/man1/i3status-rs.1

	install -Dm644 files/themes/* -t "$pkgdir"/usr/share/i3status-rust/themes/
	install -Dm644 files/icons/* -t "$pkgdir"/usr/share/i3status-rust/icons/
}

sha512sums="
9f08f92ba4935bbd844579a776716006d90f64f1c6db2c2e5cb026bed665e69673d47b086783d42b2736f0bc9ad706d81611cff9cd7ce3eff6702c28eb5d67a7  i3status-rust-v0.31.9.tar.gz
6817e3ae53054435814f282a46e6e98ac0cdec8c341978627e33e952ed354cc1afbb12ab5f4216c54a4f3b672513179811da709a5e986056c3c226ef8a4d4b20  i3status-rs-0.31.9.1
"
